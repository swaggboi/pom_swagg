#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Jul 2020

use strict;
use warnings;
use utf8;
use open qw(:std :utf8); # Fix "Wide character in print" warning
use WebService::Discord::Webhook;
#use Mozilla::CA          # Uncomment for SSL trust chain
#use Data::Dumper;        # Uncomment for debugging

# Open Webook URL file (Tom) in $HOME or $PWD
my $dotfile =
    (-r "$ENV{HOME}/.tom.url") ? "$ENV{HOME}/.tom.url" :
    (-r ".tom.url")            ? ".tom.url"            :
    die "Dotfile not found!\n";
open(my $tomUrl, $dotfile); # Open it
chomp(my $url = <$tomUrl>); # Grab 1st line

# Create webhook object
my $webhook = WebService::Discord::Webhook->new(
    url        => $url,
    verify_SSL => 1
    );

# Must specify full path for `pom`
chomp(my $pom = `/usr/games/pom`);

# Add emoji to match phase (markdown emphasis on Full or New)
$pom =
    /The Moon is New/                  ? "**$pom** 🌚" :
    /The Moon is Waxing Crescent/      ? "$pom 🌒"     :
    /The Moon is at the First Quarter/ ? "$pom 🌛"     :
    /The Moon is Waxing Gibbous/       ? "$pom 🌔"     :
    /The Moon is Full/                 ? "**$pom** 🌝" :
    /The Moon is Waning Gibbous/       ? "$pom 🌖"     :
    /The Moon is at the Last Quarter/  ? "$pom 🌜"     :
    /The Moon is Waning Crescent/      ? "$pom 🌘"     :
    $_ for $pom;

# Send it
$webhook->execute(content => $pom);

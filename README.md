# pom_swagg

Perl script I run via cron to post the phase of moon (using the pom
command) to a Discord Webhook URL

pom_swagg wants to run alongside a dot file:

- $HOME/.tom.url or .tom.url in same directory

    The Discord Webhook URL to use

This script relies on WebService::Discord::Webhook and you'll need the
`pom` command to be available at `/usr/games/pom`.

## Discord Webhook URLs

Your Discord Webhook URL should look something like this:

    https://discordapp.com/api/webhooks/740722033561960495/QZTRS0iCgFM531fY2DHWwtalOjdbyPY_waRJY8eDqeWtCTdwPNJmaPkMd1Er-a2ebD5E

I generate Webhook URLs through the Discord client application
(<http://discord.com/app> should work too) by clicking on the server
name > “Server settings” > “Integrations” > “Webhooks” > “New
Webhook”. More information on Discord Webhooks can be found
[here](https://discord.com/developers/docs/resources/webhook).

## The `pom` Command

The `pom` command typically comes bundled with a package called `bsdgames`:

### Slackware

    root@optepr0n:~/git_repos/pom_swagg# slackpkg file-search pom
    
    
    NOTICE: pkglist is older than 24h; you are encouraged to re-run 'slackpkg update'
    
    Looking for pom in package list. Please wait... /-\|DONE
    
    The list below shows the packages that contains "pom" file.
    
    [ Status           ] [ Repository               ] [ Package                                  ]
       installed               slackware64                  bsd-games-2.13-x86_64-12                  
    
    You can search specific packages using "slackpkg search package".

### Debian

    root@timecube:~# apt-file find /usr/games/pom
    bsdgames: /usr/games/pom
